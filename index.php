<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ChokoTravel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/main.css">

</head>
<body>
    <div class="wraper">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
        </div>
        <!-- Collection of nav links, forms, and other content for toggling -->
        <div class="container">
        	<div class="row">
        		<div class="bs-example">
        		    <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
								<li class="items-choco"><a href="" class="link_main"></a>
								 </li>
								<li class="items-chocomart"><a href="chocomart.kz" title="">.</a></li>
								<li class="items-chocotravel"><a href=""></a></li>
								<li class="items-optic"><a href=""></a></li>
								<li class="items-chocofood"><a href=""></a></li>
								<li class="items-doctor"><a href=""></a></li>
            </ul>
         
            <ul class="nav navbar-nav navbar-right">
                <li><button  class="registration_but type="submit" class="btn btn-default">Регистрация</button></li>
                <li><a class="enter_t" href="">Вход</a>
               
            </ul>
            </div>
        	</div>
        </div>
         </div>
    </nav>
</div>
<div class="container">
 <div class="row">
 	<div class="bs-example">
 	  <div class="col-md-2" >
 	  	<div class="btn-group">
  <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Алматы</button>
  <ul class="dropdown-menu">
    <li><a href="#">Пункт 1</a></li>
    <li><a href="#">Пункт 2</a></li>
    <li class="divider"></li>
    <li><a href="#">Пункт 3</a></li>
  </ul>
</div></div>
  <div class="col-md-2" id="col">
  	<div class="faq_block">
      	<div class="faq-icon"></div>
      	<p class="text-faq">НУЖНА ПОМОЩЬ?</p>
      </div></div>
  <div class="col-md-2" id="col"><div class="faq_block">
      	<div class="faq-icon1"></div>
      	<p class="text-faq">ЗАЩИТА ПОКУПАТЕЛЕЙ</p>
    </div></div>
  <div class="col-md-2" left>		 	<div class="faq_block">
      	<div class="faq-icon2"></div>
      	<p class="text-faq">ОБРАТНАЯ СВЯЗЬ</p>
     
    </div></div>
    		<div class="logo-wraper">
	<div class="choco-logo">
		<a href="">
			<span class="b-choco-logo"></span>
		</a>
		<p class="logo-text">
			Главное, чтобы Вы
              <br>были счастливы!
		</p>
	</div>
</div>
<div class="search-wrap">
	<div class="b-search">
		<form action="search" class="b-search-plase">
			<input type="search" placeholder="Найти среди 622 акций" class="search-input">
			<button type="submit" class="e-submit"></button>
		</form>
	</div>
</div>
</div>
<div class="banner-block">
	<span class="closer"></span>
	<div class="b-banner">
		<img src="img/banner.jpg" alt="" class="banner">
	</div>
</div>

</div>
</div>
<div class="category_nav">
	 <div class="bs-example">
	 	<div class="menu_nav_container"> 
	 		  	<ul class="menu_nav">
								<li class="category_li" ><a href="" >Все</a> </li>								      
								<li class="category_li"><a href="" >Новые</a></li>
								<li class="category_li" ><a href="">Хиты продаж</a></li>
								<li class="category_li" ><a href="">Развлечения и отдых</a></li>
								<li class="category_li" ><a href="">Красота и здоровье</a></li>
								<li class="category_li" ><a href="">Спорт</a></li>
								<li class="category_li" ><a href="">Товары</a></li>
								<li class="category_li" ><a href="">Услуги</a></li>
								<li class="category_li" ><a href="">Еда</a></li>
								<li class="category_li" ><a href="">Туризм,отели</a></li>
            </ul></div>
 	   
 </div>
</div>
<div class="b-board">
	<div class="container">
<div class="row">
	<div class="bs-example">
		<div class="block-wrapper">
	
		<div class="col-md-3"><a href=""><img src="img/under_nav.PNG"></a></div>
		<div class="col-md-3"><a href=""><img src="img/under_nav.PNG"></a></div>
		<div class="col-md-3"><a href=""><img src="img/under_nav.PNG"></a></div>
		<div class="col-md-3"><a href=""><img src="img/under_nav.PNG"></a></div>
	</div>
	</div>
</div>

	</div>
</div>
<div class="container">
	<div class="bs-example">
		<div class="b-sort">
			<div class="table-responsive">
<table class="table">
<tbody>
<tr>
<td>Сортировать:</td>
<td><input type="radio">
популярные</td>
<td><input type="radio">цена</td>
<td><input type="radio">скидка</td>
<td><input type="radio">новые</td>
<td><input type="radio">рейтинг</td>
</tr>
</tbody>
</table>
</div>
		</div>
	</div>
	
</div>
<div class="container">
	<div class="row">
		<div class="bs-example">
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a>
			</div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a></div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a></div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a>
			</div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a></div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a></div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a>
			</div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a></div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a></div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a>
			</div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a></div>
			<div class="col-md-4"><a href=""><img src="img/39540.jpg" alt=""></a></div>
		</div>
	</div>
</div>
<div class="load-more">
	<span class="e-mainpage_load-more">Показать остальные акции</span>
</div>
<div class="inf-block">
	<div class="container">
		<div class="row">
			<div class="bs-example">
			<div class="col-md-3"><div class="block-img"><img src="img/1.jpg" alt=""></div>
		</div>
		<div class="col-md-7"><h2 style="font-size: 100% !important;
    text-transform: uppercase;">Все скидки и&nbsp;акции в&nbsp;одном месте!</h2>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia excepturi maxime minus aut, vel perspiciatis cum deserunt exercitationem eos, inventore eveniet facilis assumenda quis veritatis, dolorum hic reiciendis! Unde, veritatis!</p>
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia excepturi maxime minus aut, vel perspiciatis cum deserunt exercitationem eos, inventore eveniet facilis assumenda quis veritatis, dolorum hic reiciendis! Unde, veritatis!</p>
</div>

	 </div>
		</div>
		<div class="bs-example">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia excepturi maxime minus aut, vel perspiciatis cum deserunt exercitationem eos, inventore eveniet facilis assumenda quis veritatis, dolorum hic reiciendis! Unde, veritatis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia excepturi maxime minus aut, vel perspiciatis cum deserunt exercitationem eos, inventore eveniet facilis assumenda quis veritatis, dolorum hic reiciendis! Unde, veritatis!
			</p>

		</div>
	</div>
</div>
<footer id="myFooter">
        <div class="container">
            <div class="row">
            	   <div class="col-sm-1">
                 
                </div>
                    <div class="col-sm-2">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sign up</a></li>
                        <li><a href="#">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="#">Company Information</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Help desk</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
      
                <div class="col-sm-3">
                    <div class="social-networks">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                    </div>
                 
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <p>© 2016 Copyright Text </p>
        </div>
    </footer>
</body> 
</html>